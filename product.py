# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from tryton_replication import ReplLocalIdDestMixin

__all__ = ['Product', 'Template', 'TemplateCategory', 'Category',
    'UomCategory', 'Uom', 'Configuration']


class Product(ReplLocalIdDestMixin):
    __name__ = 'product.product'
    __metaclass__ = PoolMeta


class Template(ReplLocalIdDestMixin):
    __name__ = 'product.template'
    __metaclass__ = PoolMeta

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls.list_price.required = False
        cls.cost_price.required = False
        cls.cost_price.states['required'] = False


class TemplateCategory(ReplLocalIdDestMixin):
    __name__ = 'product.template-product.category'
    __metaclass__ = PoolMeta


class Category(ReplLocalIdDestMixin):
    __name__ = 'product.category'
    __metaclass__ = PoolMeta


class UomCategory(ReplLocalIdDestMixin):
    __name__ = 'product.uom.category'
    __metaclass__ = PoolMeta


class Uom(ReplLocalIdDestMixin):
    __name__ = 'product.uom'
    __metaclass__ = PoolMeta


class Configuration(ReplLocalIdDestMixin):
    __name__ = 'product.configuration'
    __metaclass__ = PoolMeta
