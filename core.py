# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from tryton_replication import ReplLocalIdDestMixin, ReplUUIdSlavDuplexMixin

__all__ = ['Currency', 'Party', 'Company', 'User', 'Sale', 'SaleLine',
    'Address', 'PaymentTerm']


class Sale(ReplUUIdSlavDuplexMixin):
    __name__ = 'sale.sale'
    __metaclass__ = PoolMeta

    _sequenced_field = 'number'
    _notify = True

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.company.domain = []


class SaleLine(ReplUUIdSlavDuplexMixin):
    __name__ = 'sale.line'
    __metaclass__ = PoolMeta

    _notify = True


class Currency(ReplLocalIdDestMixin):
    __name__ = 'currency.currency'
    __metaclass__ = PoolMeta


class Party(ReplLocalIdDestMixin):
    __name__ = 'party.party'
    __metaclass__ = PoolMeta

    _notify = True


class Company(ReplLocalIdDestMixin):
    __name__ = 'company.company'
    __metaclass__ = PoolMeta

    active = fields.Boolean('Active')


class User:
    __name__ = 'res.user'
    __metaclass__ = PoolMeta

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._context_fields.insert(0, 'main_company')


class Address(ReplLocalIdDestMixin):
    __name__ = 'party.address'
    __metaclass__ = PoolMeta


class PaymentTerm(ReplLocalIdDestMixin):
    __name__ = 'account.invoice.payment_term'
    __metaclass__ = PoolMeta

    @classmethod
    def validate(cls, records):
        pass
