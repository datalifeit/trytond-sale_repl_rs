# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from tryton_replication import ReplDeleted, Model, Warning_
from . import core
from . import stock
from . import product


def register():
    Pool.register(
        ReplDeleted,
        Model,
        Warning_,
        core.Sale,
        core.SaleLine,
        core.Party,
        core.Company,
        core.Currency,
        core.User,
        core.Address,
        core.PaymentTerm,
        stock.Location,
        product.Product,
        product.Template,
        product.TemplateCategory,
        product.Category,
        product.UomCategory,
        product.Uom,
        product.Configuration,
        module='sale_repl_rs', type_='model')
